from django.contrib import admin

from .models import Orderable, Entity


@admin.register(Orderable)
class OrderableAdmin(admin.ModelAdmin):
    pass


@admin.register(Entity)
class EntityAdmin(admin.ModelAdmin):
    pass

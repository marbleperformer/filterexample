# Generated by Django 2.2.6 on 2019-10-20 20:25

from django.db import migrations, models
import django.db.models.deletion
import main.models.enum


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Entity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=256, null=True)),
                ('status', models.CharField(choices=[('StatusEnum.PUBLISHED', 'Published'), ('StatusEnum.DRAFT', 'Draft')], default=main.models.enum.StatusEnum('Draft'), max_length=64)),
            ],
            options={
                'verbose_name': 'Entity',
                'verbose_name_plural': 'Entities',
            },
        ),
        migrations.CreateModel(
            name='Orderable',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=256, null=True)),
                ('order', models.PositiveIntegerField(default=0)),
                ('entity', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.Entity')),
            ],
            options={
                'verbose_name': 'Orderable',
                'verbose_name_plural': 'Orderables',
            },
        ),
    ]

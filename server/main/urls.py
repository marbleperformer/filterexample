from django.urls import path

from .views import orderable_list_view


app_name = 'main'

urlpatterns = [
    path('', orderable_list_view, name='index')
]

from django.db import models

from .enum import StatusEnum

class Entity(models.Model):
    name = models.CharField(
        max_length=256, null=True, blank=True
    )
    status = models.CharField(
        max_length=64, choices=StatusEnum, default=StatusEnum.DRAFT
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Entity'
        verbose_name_plural = 'Entities'

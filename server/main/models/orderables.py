from django.db import models


class Orderable(models.Model):
    name = models.CharField(
        max_length=256, null=True, blank=True
    )
    entity = models.ForeignKey(
        'main.Entity', on_delete=models.CASCADE, null=True, blank=True
    )
    order = models.PositiveIntegerField(
        default=0
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Orderable'
        verbose_name_plural = 'Orderables'

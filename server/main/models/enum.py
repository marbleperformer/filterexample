from enum import Enum, EnumMeta


class StatusMeta(EnumMeta):
    def __iter__(cls):
        for name in cls._member_names_:
            member = cls._member_map_.get(name)
            yield str(member), member.value



class StatusEnum(Enum, metaclass=StatusMeta):
    PUBLISHED = 'Published'
    DRAFT = 'Draft'

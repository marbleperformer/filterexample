from django_filters import FilterSet
from django_filters.filters import CharFilter

from .models import Orderable


class OrderableFilterset(FilterSet):
    entity_search = CharFilter('entity', lookup_expr='name__istartswith')

    class Meta:
        model = Orderable
        fields = {
            'name': ['icontains']
        }

from django import forms


class SearchForm(forms.Form):
    search = forms.CharField(label='Search', required=False)
    name = forms.CharField(label='Name', required=False)

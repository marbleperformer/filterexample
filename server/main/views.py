from django.shortcuts import render

from .schemas import SearchSchema
from .filtersets import OrderableFilterset
from .forms import SearchForm


def orderable_list_view(request):
    schema = SearchSchema()
    form = SearchForm(data=request.GET)
    filterset = OrderableFilterset(
        data=schema.dump(request.GET)
    )
    return render(
        request, 'main/index.html',
        {
            'form':form,
            'object_list':filterset.qs
        }
    )

import re
from marshmallow import Schema, fields, pre_dump
from funcy import first


class SearchSchema(Schema):
    search = fields.Method('get_entities', data_key='entity_search')
    name = fields.String(data_key='name__icontains')

    def get_entities(self, obj):
        search = obj.get('search')
        if search:
            pattern = re.compile(r'entity[is\s-]*(\w+)')
            match = pattern.search(search)
            return first(match.groups()) if match else None

    @pre_dump
    def get_context(self, data, **kwargs):
        raw_data = list(((key, data.getlist(key)) for key in data.keys()))
        return {key: value if len(value) > 1 else first(value)
                for key, value in raw_data}
